package com.greencloud.bean;

import com.greencloud.util.FileUtil;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class GetPath {


    private String javaPath;

    private String zipfile;

    private String projectPath;

    public String getJavaPath() throws IOException {
        Properties properties = new Properties();
        InputStream in = FileUtil.class.getClassLoader().getResourceAsStream("\\config\\application.properties");
        properties.load(in);
        String packageName = (String) properties.get("generate_package_name");
        return packageName.replaceAll("\\.", "/");
    }

    public String getZipfile() {
        return FileUtil.class.getClassLoader().getResource("").getFile() + "../../src/main/zipdownload" + "\\" + System.currentTimeMillis() + ".zip";
    }

    public String getProjectPath() {
        return FileUtil.class.getClassLoader().getResource("").getFile() + "../../src/main" + "/sourcedownload/";
    }
}