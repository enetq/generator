package com.greencloud.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * Created by WangIJia on 2016/11/5.
 * 注册filter、servet、listerner
 */
@Configuration
public class ApplicationInitializer {

    @Bean
    public FilterRegistrationBean characterFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new CharacterEncodingFilter());
        registration.addInitParameter("encoding", "UTF-8");
        registration.addUrlPatterns("/*");
        registration.setOrder(2);
        return registration;
    }

    @Bean
    public FilterRegistrationBean druidStatFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new WebStatFilter());
        registration.setAsyncSupported(true);
        registration.addInitParameter("exclusions", "/static/*,*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        registration.addInitParameter("principalSessionName", "username");
        registration.addUrlPatterns("/*");
        registration.setOrder(3);
        return registration;
   }

    @Bean
    public ServletRegistrationBean druidStatServlet() {
        ServletRegistrationBean registration = new ServletRegistrationBean();
        registration.setServlet(new StatViewServlet());
        registration.setAsyncSupported(true);
        registration.addUrlMappings("/admin/monitor/druid/*");
        registration.setOrder(4);
        return registration;
    }

}
