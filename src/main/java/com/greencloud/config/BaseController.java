package com.greencloud.config;

import com.greencloud.util.ApiResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * Created by WangIJia on 2016/10/29.
 */
public class BaseController {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    
    protected static String SUPER="SUPER";

    protected final ApiResult successReturn(Object result) {

        //防止转Json直接不显示
        if(result == null) result = new Object();
        return new ApiResult(0, result);
    }

    protected final ApiResult successReturn(String msg) {
        //防止转Json直接不显示
        if(msg == null) msg = "";
        return new ApiResult(0, msg);
    }

    protected  final  ApiResult successReturn(Integer errorCode,String msg,Object result){
        if (errorCode==null){
            errorCode=0;
        }
        if (msg==null) msg="";
        if (result==null) result=new Object();
        return new ApiResult(errorCode,msg,result);

    }

    protected final ApiResult failureReturn(String msg) {
        //防止转Json直接不显示
        if(msg == null) msg = "";
        return new ApiResult(1, msg);

    }
    
    protected final ApiResult failureReturn(Integer errorCode,String msg) {
        //防止转Json直接不显示
        if(msg == null) msg = "";
        return new ApiResult(errorCode, msg);

    }

    /**获取数据库主键  uuid
     * @Author hym
     * @return
     */
    protected String getUuid(){
        return UUID.randomUUID().toString().replace("-","");
    }

    
}
