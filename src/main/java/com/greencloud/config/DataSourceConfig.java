package com.greencloud.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * Created by WangIJia on 2016/10/23.
 * 数据源配置
 */
@Configuration
// 启用注解式事务管理 <tx:annotation-driven />
@EnableTransactionManagement(proxyTargetClass = true)
public class DataSourceConfig {

    @Autowired
    private DataSourceConfigurationProperties configurationProperties;

    @Bean
    @Primary
    public DataSource dataSource() throws SQLException {
        //Using BeanUtils ...
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUrl(configurationProperties.getUrl());
        druidDataSource.setUsername(configurationProperties.getUsername());
        druidDataSource.setPassword(configurationProperties.getPassword());
        druidDataSource.setInitialSize(configurationProperties.getInitialSize());
        druidDataSource.setMinIdle(configurationProperties.getMinIdle());
        druidDataSource.setMaxActive(configurationProperties.getMaxActive());
        druidDataSource.setMaxWait(configurationProperties.getMaxWait());
        druidDataSource.setTimeBetweenEvictionRunsMillis(configurationProperties.getTimeBetweenEvictionRunsMillis());
        druidDataSource.setMinEvictableIdleTimeMillis(configurationProperties.getMinEvictableIdleTimeMillis());
        druidDataSource.setValidationQuery(configurationProperties.getValidationQuery());
        druidDataSource.setTestWhileIdle(configurationProperties.isTestWhileIdle());
        druidDataSource.setTestOnBorrow(configurationProperties.isTestOnBorrow());
        druidDataSource.setTestOnReturn(configurationProperties.isTestOnReturn());
        druidDataSource.setPoolPreparedStatements(configurationProperties.isPoolPreparedStatements());
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(configurationProperties.getMaxPoolPreparedStatementPerConnectionSize());
        druidDataSource.setFilters(configurationProperties.getFilters());
        druidDataSource.setName("hellodruid");
        return druidDataSource;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource());
        sqlSessionFactoryBean.setTypeAliasesPackage("com.greencloud");

        sqlSessionFactoryBean.setConfigLocation(new ClassPathResource("/config/mybatis/mybatis-config.xml"));
        PathMatchingResourcePatternResolver resourcePatternResolver =
                new PathMatchingResourcePatternResolver();

        sqlSessionFactoryBean.setMapperLocations(resourcePatternResolver.getResources("classpath*:com/greencloud/**/mapper/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 使用SqlSessionTemplate,不使用mapper接口
     * */
    //@Scope(value="prototype")
    @Bean
    public SqlSessionTemplate sqlSession() throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory(), ExecutorType.BATCH);
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {
        return new DataSourceTransactionManager(dataSource());
    }
}
