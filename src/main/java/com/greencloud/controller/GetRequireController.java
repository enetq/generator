package com.greencloud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/*
    获取生成需求
 */
@Controller
public class GetRequireController {

    @ResponseBody
    @RequestMapping("/getRequire")
    public String[] getRequire(HttpServletRequest request){
           String[] list=request.getParameterValues("require");
        return list;
    }

}
