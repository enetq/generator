package com.greencloud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.greencloud.service.Impl.TablesNameServiceImpl;

/*
     数据库获取所有表名
 */
@Controller
@PropertySource("config/application.properties")
public class GetTablesNameController {

    @Autowired
    private TablesNameServiceImpl tablesNameServiceImpl;

    @Value("${table_schema}")
    private String table_schema;

    @ResponseBody
    @RequestMapping("/getTables")
    public List<String> findAllTables() {
        List<String> list = new ArrayList<>();
        list = tablesNameServiceImpl.findAllTablesName(table_schema);
        return list;
    }
}
