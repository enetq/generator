package com.greencloud.controller;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greencloud.service.Impl.ColumnsNameServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.greencloud.bean.ColumnDefinition;
/*
    获取所选表名查询对应字段
 */

@Controller
public class GetSelectValueController {

    @Autowired
    private ColumnsNameServiceImpl columnsNameService;

    @ResponseBody
    @RequestMapping("/getColumns")
    public Map<String,  List<ColumnDefinition>> GetSelectValue(HttpServletRequest request){
        Map<String, List<ColumnDefinition>> map=new HashMap<>();
        String[] tablesNames=request.getParameterValues("tablesName");
        for (String tableName:tablesNames)
        {
            map.put(tableName,columnsNameService.findAllColumnsFromTable(tableName));
        }
        return map;
    }



}
