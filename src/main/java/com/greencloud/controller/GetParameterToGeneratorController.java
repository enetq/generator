package com.greencloud.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.greencloud.service.Impl.ColumnsNameServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.greencloud.bean.ColumnDefinition;
import com.greencloud.bean.GetPath;
import com.greencloud.service.ColumnsNameService;
import com.greencloud.util.FileUtil;
import com.greencloud.util.GeneratorUtil;

/*
        将获得的表名，字段，需求生成
 */

@Controller
public class GetParameterToGeneratorController {


    @Autowired
    private ColumnsNameServiceImpl columnsNameService;

    @ResponseBody
    @RequestMapping("/getParameterToGenerator")
    public void ToGenerator(HttpServletRequest request) throws Exception {
        String[] tablesNames = request.getParameterValues("tablesName");
        String[] requires = request.getParameterValues("require");
        //若有文件存在，则删除、避免重复生成
        GetPath getPath=new GetPath();
        FileUtil.delFolder(getPath.getProjectPath()+getPath.getJavaPath());
        for (String tableName : tablesNames) {
            List<ColumnDefinition> Values = columnsNameService.findAllColumnsFromTable(tableName);
            GeneratorUtil.execute(tableName, Values, requires);
        }
        //压缩生成文件
        FileUtil.zipDownload();
    }


}