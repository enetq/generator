package com.greencloud.mapper;


import com.greencloud.bean.ColumnDefinition;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
@Mapper
public interface ColumnsNameMapper {

    @Select("select distinct b.column_name,b.data_type,b.column_key,b.column_comment,a.table_comment from information_schema.tables a left join  information_schema.columns b ON a.table_name = b.table_name where a.table_name = #{tableName} ")
    List<ColumnDefinition> findAllColumnsFromTable(String tableName);


}
