package com.greencloud.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
@Mapper
@Component
public interface TablesNameMapper   {
    @Select("select table_name from information_schema.tables where table_schema= #{table_schema} and table_type='base table'")
    List<String> findAllTablesName(String table_schema);

}
