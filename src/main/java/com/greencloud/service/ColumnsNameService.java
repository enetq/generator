package com.greencloud.service;


import com.greencloud.bean.ColumnDefinition;
import java.util.List;

public interface ColumnsNameService {

     List<ColumnDefinition> findAllColumnsFromTable(String tableName);

}
