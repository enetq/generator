package com.greencloud.service.Impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greencloud.bean.ColumnDefinition;
import com.greencloud.mapper.ColumnsNameMapper;
import com.greencloud.service.ColumnsNameService;

@Service
public class ColumnsNameServiceImpl implements ColumnsNameService {

    @Autowired
    private  ColumnsNameMapper columnsNameMapper;


    public List<ColumnDefinition> findAllColumnsFromTable(String tableName) {
        List<ColumnDefinition> list = columnsNameMapper.findAllColumnsFromTable(tableName);
        return list;
    }
}
