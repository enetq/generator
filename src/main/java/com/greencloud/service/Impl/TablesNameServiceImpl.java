package com.greencloud.service.Impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.greencloud.mapper.TablesNameMapper;
import com.greencloud.service.TablesNameService;

@Service
public class TablesNameServiceImpl implements TablesNameService {

    @Autowired
    private TablesNameMapper tablesNameMapper;

    @ResponseBody
    public List<String> findAllTablesName(String table_schema) {
     List<String> list = tablesNameMapper.findAllTablesName(table_schema);
     return  list;
    }

}
