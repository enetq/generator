package com.greencloud.service;




import java.util.List;

public interface TablesNameService {

    List<String> findAllTablesName(String table_schema);

}
