package com.greencloud.util;


import com.greencloud.bean.ColumnDefinition;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.web.bind.annotation.RequestMapping;


import java.io.*;
import java.sql.SQLException;
import java.util.*;

import static com.greencloud.util.GeneratorUtil.fillData;

/*
    模板读取工具
 */

public class GeneratorUtil {


    public static void execute(String tablesName, List<ColumnDefinition> Values, String[] requires) throws IOException, SQLException {
        Properties properties = new Properties();
        InputStream in = GeneratorUtil.class.getClassLoader().getResourceAsStream("\\config\\application.properties");

        properties.load(in);
        String packageName = (String) properties.get("generate_package_name");
        String utilName = (String) properties.get("generate_util_name");
        String projectName = (String) properties.get("project_name");//工程名

        String templatePath = (String) properties.get("templatePath");

        fillData(FieldUtil.processTable2Java(tablesName), Values, requires, "velocity/template/" + templatePath + "/", packageName,utilName,projectName);


    }


    public static void fillData(String javaName, List<ColumnDefinition> Values, String[] requires, String templateUrl, String packageName,String utilName,String projectName) {

        // 3、填充模板数据
        VelocityContext context = new VelocityContext();
        context.put("javaBeanName", javaName);
        context.put("packageName", packageName);//包名
        context.put("utilName", utilName);//工具类名
        context.put("projectName", projectName);//工程名

        context.put("tableName", javaName);
        context.put("Values", Values);
        context.put("SqlTypeClass", SqlTypeUtil.class);
        context.put("FieldUtil", FieldUtil.class);
        context.put("classNameUpCase", javaName);//类名大写

        String firstLowerJava = javaName.substring(0,1).toLowerCase()+javaName.substring(1);

        context.put("classNameLowerCase", firstLowerJava);//类名小写

        Date date = new Date();
        String nowStr = DateUtil.formatDateWithPattern(date, "yyyy-MM-dd HH:mm:ss:SSS");
        context.put("nowTime", nowStr);//生成时间

        String javaPath = packageName.replaceAll("\\.", "/");
        String rootPath = GeneratorUtil.class.getClassLoader().getResource("").getFile() + "../../src/main";
        String projectPath=rootPath + "/sourcedownload/" +javaPath;
        

        // 1、velocity模板设置、引擎初始化
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

        ve.setProperty("input.encoding", "UTF-8");
        ve.setProperty("output.encoding", "UTF-8");
        ve.init();
        if (requires != null) {
            List<String> list = Arrays.asList(requires);
            try {
                if (list.contains("Dao")) {
                    proceeDao(javaName, templateUrl, context, projectPath, ve);
                }

                if(list.contains("Mapper")) {
                    processMapper(javaName, templateUrl, context, projectPath, ve);
                }

                if (list.contains("Service")) {
                    processService(javaName, templateUrl, context, projectPath, ve);
                }

                if (list.contains("Entity")) {
                    Template mapperTemplate = ve.getTemplate(templateUrl + "Entity.vm");
                    FileUtil.generate(mapperTemplate, context, projectPath + "/entity/" + javaName + ".java");
                }

                if (list.contains("Action")) {
                    Template mapperTemplate = ve.getTemplate(templateUrl + "ActionTemplate.vm");
                    FileUtil.generate(mapperTemplate, context, projectPath + "/controller/" + javaName + "Controller.java");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private static void processService(String javaName, String templateUrl, VelocityContext context, String projectPath, VelocityEngine ve) throws IOException {
        Template serviceTemplate = ve.getTemplate(templateUrl + "ServiceTemplate.vm");
        Template serviceImplTemplate = ve.getTemplate(templateUrl + "ServiceImplTemplate.vm");

        FileUtil.generate(serviceTemplate, context, projectPath + "/service/" + javaName + "Service.java");
        FileUtil.generate(serviceImplTemplate, context, projectPath + "/service/impl/" + javaName + "ServiceImpl.java");
    }

    private static void processMapper(String javaName, String templateUrl, VelocityContext context, String projectPath, VelocityEngine ve) throws IOException {
        Template mapperTemplate = ve.getTemplate(templateUrl + "Mapper.vm");
        FileUtil.generate(mapperTemplate, context, projectPath + "/mapper/" + javaName + ".xml");
    }

    private static void proceeDao(String javaName, String templateUrl, VelocityContext context, String projectPath, VelocityEngine ve) throws IOException {
        Template daoTemplate = ve.getTemplate(templateUrl + "DaoTemplate.vm");
        Template daoImplTemplate = ve.getTemplate(templateUrl + "DaoImplTemplate.vm");
        FileUtil.generate(daoTemplate, context, projectPath + "/dao/" + javaName + "Dao.java");
        FileUtil.generate(daoImplTemplate, context, projectPath + "/dao/impl/" + javaName + "DaoImpl.java");
    }

}
