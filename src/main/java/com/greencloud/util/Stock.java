package com.greencloud.util;

/**
 * @description:
 * @author: 王佳  15:23 2019/2/18
 * @version: 1.0
 * @modify: MODIFIER'S NAME YYYY/MM/DD 修改内容简述
 * @Copyright: 版权信息
 */
public class Stock {

    public static void main(String args[]){

        //初始资金
        double m = 1;
        //最终资金
        double n = 0;

        for(int i=0;i<2500;i++){
            n = m+m*0.06;
            m = n;
            System.out.println(i+":"+m);
        }
        System.out.println(n);
    }
}
