package com.greencloud.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Date date = new Date();
		//DateUtil.formatDateWithPattern(date, "yyyy-MM-dd HH:mm:ss:SSS");
		//System.out.println(DateUtil.dateToTimeStamp(date));
		
		System.out.println(DateUtil.formatTimeStampWithPattern(date,"yyyy-MM-dd HH:mm:ss"));
	}

	/**
	 * 返回指定的日期指定的日期格式
	 * @param date
	 *            要转换的日期对象
	 * @param pattern
	 *            返回的日期字符串的格式
	 * yyyy-MM-dd HH:mm:ss:SSS
	 * @return dateStr 
	 * 			     转换结果
	 * */
	public static String formatDateWithPattern(Date date, String pattern) {
		if(date!=null&&pattern!=null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String dateStr = simpleDateFormat.format(date);
			return dateStr;
		}else {
			return null;
		}
		
	}
	
	/**
	 * Date类型数据转换成TimeStamp类型
	 * */
	public static Timestamp dateToTimeStamp(Date date){
		if(date!=null){
			Timestamp ts = new Timestamp(date.getTime());
			return ts;
		}else {
			return null;
		}
		
	}
	
	
	public static Timestamp formatTimeStampWithPattern(Date date, String pattern) {
		if(date!=null&&pattern!=null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String dateStr = simpleDateFormat.format(date);
			Timestamp.valueOf(dateStr);
			return Timestamp.valueOf(dateStr);
		}else {
			return null;
		}
		
	}

}
